
/*Creacion de la base de datos*/
CREATE DATABASE database_links;
USE database_links;

/*Creacion de la tabla: procesos*/
CREATE TABLE procesos(
    id int(11) not null AUTO_INCREMENT,
    titulo varchar(100),
    descripcion text,
    PRIMARY KEY (id)
);
/*Comprobacion*/
DESCRIBE procesos;

/*Creacion de la tabla: trabajadores*/
CREATE TABLE trabajadores(
    id int(11) not null AUTO_INCREMENT,
    nombre varchar(50),
    apellidos varchar(100),
    usuario varchar(50) not null,
    contraseña varchar(60) not null,
    horasmax int(11) not null,
    horasacum int(11),
    PRIMARY KEY (id)
);
/*Comprobacion*/
DESCRIBE trabajadores;

/*Creacion de la tabla: etapas*/
CREATE TABLE etapas(
    id int(11) not null AUTO_INCREMENT,
    titulo varchar(50) not null,
    descripcion text,
    horas int(11) not null,
    PRIMARY KEY (id)
);
/*Comprobacion*/
DESCRIBE etapas;

/*Tablas compuestas*/
/*Creacion de la tabla: trabajadores por etapa*/
CREATE TABLE trabajadores_etapa(
    id int(11) not null,
    idetapa int(11) not null,
    idtrabajador int(11) not null,
    PRIMARY KEY (id)
);
/*Comprobacion*/
DESCRIBE trabajadores_etapa;

/*Creacion de la tabla: Etapas en un proceso*/
CREATE TABLE etapas_proceso (
    id int(11),
    idetapa int(11) not null,
    idproceso int(11) not null,
    PRIMARY KEY (id)
); 
/*Comprobacion*/
DESCRIBE etapas_proceso;

/*Pruebas varias*/


/*
CREATE TABLE users(
    id INT(11) NOT NULL,
    username VARCHAR(16) NOT NULL,
    password VARCHAR(60) NOT NULL,
    fullname VARCHAR(100) NOT NULL
);

ALTER TABLE users
    ADD PRIMARY KEY (id);

ALTER TABLE etapas_proceso MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;


DESCRIBE users;

CREATE TABLE links(
    id INT(11) PRIMARY KEY NOT NULL,
    title VARCHAR(150) NOT NULL,
    url VARCHAR(255) NOT NULL,
    descripcion TEXT,
    user_id INT(11) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)   

);
ALTER TABLE links
MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

DESCRIBE links;*/