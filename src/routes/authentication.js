//Import de la app
const express = require('express');
//Import del enrutador
const router = express.Router();
//Include de los metodos de autenticacion
const passport = require('passport');
//Funciones auxiliares de autenticacion
const auth = require('../lib/auth');
//Ruta para crear usuario
router.get('/signup', auth.IsNotLoggedIn, (req, res) => {
    console.log(req.body);
    //Renderiza esto
    res.render('auth/signup');
});

//router.post('/signup', (req, res) => {
//    passport.authenticate('local.signup', {
//        successRedirect: '/profile',
//        failureRedirect: '/signup',
//        failureFlash: true
//    });
//
//});
//
//Arriba y abajo hace lo mismo (abrebiatura)
//Post de la ruta anterior creacion de usuarios
router.post('/signup', passport.authenticate('local.signup', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    failureFlash: true
}));
//Ruta para el acceso a la aplicacion
router.get('/signin', auth.IsNotLoggedIn, (req, res) => {
    res.render('auth/signin');
});
//Post de acceso a la aplicacion
router.post('/signin', (req, res, next) => {
    passport.authenticate('local.signin', {
        successRedirect: '/profile',
        failureRedirect: '/signin',
        failureFlash: true
    })(req, res, next);
});
//Vista de bienvenida
router.get('/profile', auth.IsLoggedIn,  (req, res) => {

    res.render('profile');
});
//Logout 
router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/signin');
});

module.exports = router;