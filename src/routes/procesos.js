//Import de la app
const express = require('express');
//Import del enrutador
const router = express.Router();
//Importo la conexion
const pool = require('../database');
//Include del metodo para comprobar login
const { IsLoggedIn } = require('../lib/auth');

//Ruta para añadir precesos
router.get('/add', IsLoggedIn, (req, res) => {
    res.render('procesos/add');
});

//Post de la ruta anterior. Crea nuevo proceso
router.post('/add', IsLoggedIn, async (req, res) => {
    //Recupero los datos
    const{ titulo, descripcion } = req.body;
    //Creo el objeto a insertar
    const newLink = {
        titulo,
        descripcion
    };
    //INSERT
    await pool.query('INSERT INTO procesos set ?', [newLink]);
    //MSG
    req.flash('success', 'Link guardado satisfactoriamente');
    //Redirect
    res.redirect('/procesos');
});

//Ruta raiz de procesos. Lista todos los procesos
router.get('/', IsLoggedIn, async (req, res) => {
    //Consulto la informacion de todos los procesos
    const links = await pool.query('SELECT * FROM procesos');
    //Render con objeto
    res.render('procesos/list', {links});
});

//Ruta para borrar el proceso con id igual al que le pasemos en la url
router.get('/delete/:id', IsLoggedIn, async (req, res) => {
    //Recupero dato
    const{ id } = req.params;
    //DELETE
    await pool.query('DELETE FROM procesos WHERE ID = ?', [id]);
    //MSG
    req.flash('success', 'Proceso quitado');
    //Redirect
    res.redirect('/procesos');
});

//Ruta para editar el proceso seleccionado
router.get('/edit/:id', IsLoggedIn, async (req, res) => {
    //Recupero idproceso
    const{ id } = req.params;
    //Consulto los datos del proceso seleccionado
    const links = await pool.query('SELECT * FROM procesos WHERE id=?', [id]);
    //Render con objeto
    res.render('procesos/edit', {links: links[0]});
});

//Post de la ruta anterior. Machaca los datos del proceso seleccionado con los del formulario
router.post('/edit/:id', IsLoggedIn, async (req, res) => {
    //Recupero idproceso
    const { id } = req.params;
    //Recupero los datos del formulario
    const{ titulo, descripcion } = req.body;
    //Construyo el objeto
    const newLink = {
        titulo,
        descripcion
    };  
    //UPDATE  
    await pool.query('UPDATE procesos set ? WHERE id = ?', [newLink, id]);
    //MSG
    req.flash('success', 'Proceso actualizado');
    //Redirect
    res.redirect('/procesos');
});

module.exports = router;