//Import de la app
const express = require('express');
//Import del enrutador
const router = express.Router();
//Importo la conexion
const pool = require('../database');
//Funcion auxiliar para comprobar el logueo
const { IsLoggedIn } = require('../lib/auth');

//Ruta raiz
router.get('/', IsLoggedIn, async (req, res) => {
    //Consulta los procesos
    const links = await pool.query('SELECT * FROM procesos');
      
    //Pasa los datos a la siguiente vista y renderiza
    res.render('admin2/lista', {links});
});


//Ruta para asignar o desasignar una etapa a un proceso
router.get('/etapacion/:idproceso', IsLoggedIn, async (req, res) => {
    //Recupero el id del proceso elegido en la vista anterior
    const{ idproceso } = req.params;
    //Consulto todas las etapas que esten aun sin asignar
    const etapas = await pool.query('SELECT * FROM etapas WHERE id NOT IN (SELECT idetapa FROM etapas_proceso WHERE idproceso = ?)',[idproceso]);
    //Consulto el resto de la info del proceso
    const proceso = await pool.query('SELECT * FROM procesos WHERE id=?', [idproceso]);
    var resultArray = Object.values(JSON.parse(JSON.stringify(etapas)));
    var resultArray2 = Object.values(JSON.parse(JSON.stringify(proceso)));

    //Construyo el objeto a pasar a la siguiente vista
    resultArray.forEach(function(v){ 
        v.idproceso = idproceso, 
        v.titulo2 = resultArray2[0].titulo,
        v.descripcion2 = resultArray2[0].descripcion
    });
    
    res.render('admin2/etapacion', {resultArray}); 
    
});

//Post del get anterior
router.post('/etapacion', IsLoggedIn, async (req, res) => {
    //Recupero datos
    const {idetapa} = req.body;  
    const {idproceso} = req.body;
    
    //Construyo objeto a insertar
    const asignacion = {
        idetapa,
        idproceso
    };
    //INSERT
    await pool.query('INSERT INTO etapas_proceso set ?', [asignacion]);
    //MSG
    req.flash('message', 'Etapa actualizado');
    //Redirect al inicio
    res.redirect('/admin2');
});

//Ruta para borrar asignacion
router.get('/delete/:id', IsLoggedIn, async (req, res) => {
    //Recupero id de proceso
    const{ id } = req.params;
    
    //Consulto todos los datos de etapas asignadas a el proceso en cuestion
    etapas = await pool.query('SELECT * FROM etapas WHERE id IN (SELECT idetapa FROM etapas_proceso WHERE idproceso = ?)',[id]);
    console.log(etapas);
    //Recupero toda la info del proceso
    proceso = await pool.query('SELECT * FROM procesos WHERE id=?', [id]);
    console.log(proceso);
    
    var resultArray = Object.values(JSON.parse(JSON.stringify(etapas)));
    var resultArray2 = Object.values(JSON.parse(JSON.stringify(proceso)));
    
    //Construccion del objeto para la vista
    resultArray.forEach(function(v){ 
        v.idproceso = id, 
        v.titulo2 = resultArray2[0].titulo,
        v.descripcion2 = resultArray2[0].descripcion
    });

    //Paso el objeto y renderizo
    res.render('admin2/delete',{resultArray});
});

//Esta es la ruta para el boton de borrar (La que borra de verdad)
router.get('/deleted/:id/:idproceso', IsLoggedIn, async (req, res) => {
    //Recuperro datos
    const{ id } = req.params;
    const{ idproceso } = req.params;
    console.log('Idpetapa: ' + id + 'idproceso' + idproceso);
    //DELETE
    await pool.query('DELETE FROM etapas_proceso WHERE idproceso = ? and idetapa = ?', [idproceso, id]);
    //MSG
    req.flash('message', 'Etapa quitada');
    //Redirecciono
    res.redirect('/admin2');
}); 

module.exports = router;