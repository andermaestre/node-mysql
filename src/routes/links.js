//Import de la app
const express = require('express');
//Import del enrutador
const router = express.Router();
//Importo la conexion
const pool = require('../database');
//Include del metodo para comprobar login
const { IsLoggedIn } = require('../lib/auth');

//Ruta para añadir etapa
router.get('/add', IsLoggedIn, (req, res) => {
    
    res.render('links/add');
});

//router.post('/add', (req, res) =>{
//    res.send('Recibido');
//});

//Post de la vista anterior. Añade nueva etapa
router.post('/add', IsLoggedIn, async (req, res) => {
    //Recupero la informacion de la vista
    const{ titulo, horas, descripcion } = req.body;
    //Construyo el objeto
    const newLink = {
        titulo,
        horas,
        descripcion
    };
    //INSERT
    await pool.query('INSERT INTO etapas set ?', [newLink]);
    //MSG
    req.flash('success', 'Link guardado satisfactoriamente');
    //Redireccion
    res.redirect('/links');
});

//Ruta raiz de links. Enseña las etapas creadas
router.get('/', IsLoggedIn, async (req, res) => {
    //Consulta: toda la informacion de todas las etapas
    const links = await pool.query('SELECT * FROM etapas');
    //Renderiza con objeto
    res.render('links/list', {links});
});

//Ruta para borrar una etapa
router.get('/delete/:id', IsLoggedIn, async (req, res) => {
    //Recupero la info de la vista
    const{ id } = req.params;
    //Borro el campo cuyo id me han pasado
    await pool.query('DELETE FROM etapas WHERE ID = ?', [id]);
    //MSG
    req.flash('success', 'Enlace quitado');
    //Redirect
    res.redirect('/links');
});

//Ruta para editar etapas
router.get('/edit/:id', IsLoggedIn, async (req, res) => {
    //Recupero la info de la vista
    const{ id } = req.params;
    //Consulto la informacion de la etapa seleccionada
    const links = await pool.query('SELECT * FROM etapas WHERE id=?', [id]);
    //Render con objeto
    res.render('links/edit', {links: links[0]});    
});

//Post de la ruta anterior. Guarda los datos del formulario y machaca los de la base de datos
router.post('/edit/:id', IsLoggedIn, async (req, res) => {
    //Recupero el dato que necesito de la url
    const { id } = req.params;
    //Recupero los datos del form
    const{ titulo, horas, descripcion } = req.body;
    //Construyo el objeto
    const newLink = {
        titulo,
        horas,
        descripcion
    };    
    //INSERT
    await pool.query('UPDATE etapas set ? WHERE id = ?', [newLink, id]);
    //MSG
    req.flash('success', 'Etapa actualizado');
    //Redirect
    res.redirect('/links');
});

module.exports = router;