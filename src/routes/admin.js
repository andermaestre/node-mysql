//Import de la app
const express = require('express');
//Import del enrutador
const router = express.Router();
//Importo la conexion
const pool = require('../database');
//Funcion auxiliar para comprobar el logueo
const { IsLoggedIn } = require('../lib/auth');
//Ruta raiz de admin
router.get('/', IsLoggedIn, async (req, res) => {
    //Consulto las etapas
    const links = await pool.query('SELECT * FROM etapas');
    //Y se las paso a la vista
    res.render('admin/trabetap', {links});
});


//Ruta para asignar trabajadores a etapa (No Lo asigna a los turnos sino a la tabla de peticiones)
router.get('/adminworkers/:idetapa', IsLoggedIn, async (req, res) => {
    //Recupero el id de la etapa seleccionada en la vista anterior
    const{ idetapa } = req.params;
    //Informacion de los trabajadores que no esten asignados en la etapa seleccionada
    const disponibles = await pool.query('SELECT * FROM trabajadores WHERE id NOT IN (SELECT idtrabajador FROM peticiones WHERE idetapa != ?)',[idetapa]);
    //Recupero la info de la etapa
    const etapas = await pool.query('SELECT * FROM etapas WHERE id=?', [idetapa]);
    
    var resultArray = Object.values(JSON.parse(JSON.stringify(disponibles)));
    var resultArray2 = Object.values(JSON.parse(JSON.stringify(etapas)));
    //Incrusto la informacion de la etapa en el objeto que voy a pasar a la vista
    resultArray.forEach(function(v){ 
        v.idetapa = idetapa, 
        v.titulo = resultArray2[0].titulo,
        v.horas = resultArray2[0].horas
    });
    
    
    //console.log(disponibles);
    //disponibles.idetapa = idetapa;
    //console.log(resultArray);
    res.render('admin/adminworkers', {resultArray}); 
    
});
//Post de la vista anterior
router.post('/adminworkers', IsLoggedIn, async (req, res) => {
    //console.log(req.body);
    //Recupero los datos
    const {id} = req.body;  
    const {idetapa} = req.body;
    
    //Creo el objeto
    const turno = {
        idtrabajador: id,
        idetapa
    };
    //Inserto el objeto
    await pool.query('INSERT INTO peticiones set ?', [turno]);
    //MSG
    req.flash('message', 'Etapa actualizado');
    //Redireccion a otra vista
    res.redirect('/admin');
});

//Ruta para borrar asignacion
router.get('/delete/:id', IsLoggedIn, async (req, res) => {
    //Recupero id de proceso
    const{ id } = req.params;
    
    //Consulto todos los datos de trabajadores asignados a la etapa en cuestion
    etapas = await pool.query('SELECT * FROM trabajadores WHERE id IN (SELECT idetapa FROM trabajadores_etapa WHERE idetapa = ?)',[id]);
    console.log(etapas);
    //Recupero toda la info del proceso
    proceso = await pool.query('SELECT * FROM etapas WHERE id=?', [id]);
    console.log(proceso);
    
    var resultArray = Object.values(JSON.parse(JSON.stringify(etapas)));
    var resultArray2 = Object.values(JSON.parse(JSON.stringify(proceso)));
    
    //Construccion del objeto para la vista
    resultArray.forEach(function(v){ 
        v.idetapa = id, 
        v.titulo2 = resultArray2[0].titulo,
        v.descripcion2 = resultArray2[0].descripcion
    });

    //Paso el objeto y renderizo
    res.render('admin/delete',{resultArray});
});

//Esta es la ruta para el boton de borrar (La que borra de verdad)
router.get('/deleted/:id/:idetapa', IsLoggedIn, async (req, res) => {
    //Recuperro datos
    const{ id } = req.params;
    const{ idetapa } = req.params;
    console.log('Idpetapa: ' + id + 'idproceso' + idetapa);
    //DELETE
    await pool.query('DELETE FROM etapas_proceso WHERE idetapa = ? and idtrabajador = ?', [idetapa, id]);
    //MSG
    req.flash('message', 'Etapa quitada');
    //Redirecciono
    res.redirect('/admin');
}); 

module.exports = router;