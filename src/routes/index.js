//Import de la app 
const express = require('express');
//Import del enrutador
const router = express.Router();
//Pagina de inicio
router.get('/', (req, res) => {
    res.render('index');
});


module.exports = router;