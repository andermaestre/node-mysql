//Modulos necesarios para crear la conexion
const mysql = require('mysql');
//Para async
const{promisify} = require('util');
const { database } = require('./keys');
//Conexion en si
const pool = mysql.createPool(database);

pool.getConnection((err, connection) => {
    if(err){
        if(err.code === 'PROTOCOL_CONNECTION_LOST'){
            console.error('Conexion con la base de datos perdida');
        }
        if(err.code === 'ER_CON_COUNT_ERROR'){
            console.error('Demasiadas conexiones a la base de datos');
        }
        if(err.code === 'ECONNREFUSED'){
            console.error('No se te concede acceso mortal');
        }
    }

    if(connection) {
        connection.release();
    }
    console.error('Conectado y funcionando');
    return;
});

//Permite query async
pool.query = promisify(pool.query);
//Devuelve la conexion
module.exports = pool;