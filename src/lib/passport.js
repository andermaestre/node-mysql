//Include del modulo para login
const passport = require('passport');
//Include del modulo para crear el metodo de login
const LocalStrategy = require('passport-local').Strategy;
//Include del modulo para realizar consultas
const pool = require('../database');
//Include del modulo con los metodos para encriptar y comparar contraseñas
const helpers = require('../lib/helpers');

//Logueo de usuario
passport.use('local.signin', new LocalStrategy({
    usernameField: 'usuario',
    passwordField: 'contraseña',
    passReqToCallback: true
}, async (req, usuario, contraseña, done) => {
    //console.log(req.body);
    //console.log(usuario);
    //console.log(contraseña);
    //Recuperacion de datos de usuario
    const rows = await pool.query('SELECT * FROM trabajadores WHERE usuario = ?', [usuario]);
    //Si hay alguien
    if(rows.length > 0){
        const user = rows[0];
        //Comprueba la contraseña
        const validPassword = await helpers.matchPassword(contraseña, user.contraseña);
        //Si es correcta
        if(validPassword){
            //Correcto
            done(null, user, req.flash('success', 'Welcome ' + user.usuario));
        }else{
            //Contraseña equivocada
            done(null, false, req.flash('message', 'Contraseña chunga'));
        }
    //Si no hay usuario
    }else{        
        return done(null, false, req.flash('message', 'El usuario no existe'));
    }
}));
//Creacon de usuario
passport.use('local.signup', new LocalStrategy({
    usernameField: 'usuario',
    passwordField: 'contraseña',
    passReqToCallback: true
}, async (req, usuario, contraseña, done) => {
    //Recupero la informacion de la vista
    const {nombre} = req.body;
    const {apellidos} = req.body;
    const {horasmax} = req.body;
    const {horasacum} = req.body;
    //Construccion de item para los datos introducidos
    const newUser = {
        usuario,
        contraseña,
        nombre,
        apellidos,
        horasmax,
        horasacum
    };
    //Encriptado de la contraseña antes de guardar
    newUser.contraseña = await helpers.encryptPassword(contraseña);
    const result = await pool.query('INSERT INTO trabajadores SET ?', [newUser]);
    newUser.id = result.insertId;
    console.log(result);
    return done(null, newUser);
    
}));
//Mantener sesion iniciada
passport.serializeUser((usr, done) => {
    done(null, usr.id);
});
//Cerrar sesion
passport.deserializeUser( async (id, done) => {
    const rows = await pool.query('SELECT * FROM trabajadores WHERE id = ?', [id])
    done(null, rows[0]);
});
