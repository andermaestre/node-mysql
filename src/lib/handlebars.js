//Include del modulo que transforma fecha en tiempo transcurrido desde fecha
const {format} = require('timeago.js');

//Utilidades para encriptar y comparar contraseña
const helpers = {};
//Funcion que transforma fecha en tiempo transcurrido desde fecha
helpers.timeago = (timestamp) => {
    return format(timestamp);
};
//Orden que exporta los metodos
module.exports = helpers;