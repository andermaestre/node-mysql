//Include de pool para hacer consultas a la base de datos
const pool = require('../database');
//Funciones relacionadas con la autenticacion
module.exports = {
    //Funcion que comprueba si el usuario esta logueado
    IsLoggedIn(req, res, next){
        if(req.isAuthenticated()){
            return next();
        }
        return res.redirect('/signin');
        
    },
    //Funcion que comprueba si el usuario no esta logueado
    IsNotLoggedIn(req, res, next){
        if(!req.isAuthenticated()){
            return next();
        }
        return res.redirect('/profile');
        
    }
};