//Include del modulo para encriptar
const bcrypt = require('bcryptjs');
const helpers = {};

//Metodo para encriptar contraseña
helpers.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(5);
    const hash = await bcrypt.hash(password, salt);
    return hash;
};

//Metodo para comparar contraseña encriptada
helpers.matchPassword = async (password, savedPassword) => {
    try{
        return await bcrypt.compare(password, savedPassword);
    }catch(e){
        console.log(e);
    }
    
};

module.exports = helpers;