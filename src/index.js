const express = require('express');
const morgan = require('morgan');
const exphbs = require('express-handlebars')
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const MySQLStore = require('express-mysql-session');
const {database} = require('./keys');
const passport = require('passport');

//Inicializar express y autentificacion
const app = express();
require('./lib/passport');

//Settings :
    //Puerto
    //Vistas
    //Motor de renderizado -> Handdlebars

app.set('port', 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs', 
    helpers: require('./lib/handlebars')
}));
//Extension que va a leer handdlebars
app.set('view engine', '.hbs');

//Middlewares

//Sesion para passport (autenticacion)
app.use(session({
    secret: 'mysqlsession',
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore(database)
}));
//Mensajes por pantalla
app.use(flash());
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session());


//Variables Globales
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.success = req.flash('message');
    app.locals.user = req.user;
    next();
});

//Rutas 

//Carpeta donde estan los archivos js que configuran las rutas
app.use(require('./routes'));
app.use(require('./routes/authentication'));
app.use('/links',require('./routes/links'));
app.use('/procesos',require('./routes/procesos'));
app.use('/admin',require('./routes/admin'));
app.use('/admin2',require('./routes/admin2'));

//Carpeta Public
app.use(express.static(path.join(__dirname, 'public')));

//Start the server
app.listen(app.get('port'), () =>{
    console.log('Server on port', app.get('port'));
})